import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.text.DecimalFormat;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

@SuppressWarnings("serial")
public class CurrencyPanel extends JPanel {
	// combo-box that allows user to select required conversion
	private final static String[] currencyList = { "Euro (EUR)", "US Dollars (USD)", "Australian Dollars (AUD)",
			"Canadian Dollars (CAD)", "IceLandic Krona (ISK)", "United Arab Emirates Dirham (AED)",
			"South African Rand (ZAR)", "Thai Baht (THB)" };
	/***
	 * Variable declare and import the swing compoents
	 */
	double num1, num2;
	static String symbol;
	@SuppressWarnings("rawtypes")
	static JComboBox currencyOption;
	public CurrencyPanel currencyPanel;
	static JLabel currenyResult, currencyPound;
	static JTextField currencyInput;
	public static JButton currencyButton;
	public MainPanel mainPanel;

	public static boolean fileValidation = false; // whether valid file has been or not check
	public static int validUser = 0; // count number of valid files uploaded by user.

	@SuppressWarnings({ "unchecked", "rawtypes" })
	CurrencyPanel() {
		/* adding comboBox, buttons,labels and textField to the panel */
		@SuppressWarnings("unused")
		ActionListener listener = new ConvertListener();
		currencyOption = new JComboBox(currencyList);
		currencyOption.addActionListener(listener);
		currencyOption.setToolTipText("Choose the currency you want to use for future references");
		currenyResult = new JLabel("----");
		currenyResult.setToolTipText("Result of currency conversion");
		currencyInput = new JTextField(6);
		currencyInput.addActionListener(listener);
		currencyInput.setToolTipText("Enter a numeric value ");
		currencyButton = new JButton("Convert");
		currencyButton.addActionListener(listener);
		currencyButton.setToolTipText("Click button for currency conversion");
		currencyButton.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					currencyButton.doClick();
				}
			}
		});
		/* currency label for enter value */
		JLabel currencyLabel = new JLabel("Enter Value : ");
		currencyPound = new JLabel("British Pound (GBP)");
		add(currencyPound);
		add(currencyOption);
		add(currencyLabel);
		add(currencyInput);
		add(currencyButton);
		add(currenyResult);
	}

	private class ConvertListener implements ActionListener {
		public void actionPerformed(ActionEvent ex) {
			String inputText = currencyInput.getText().trim();
			boolean checkReverse = true;
			if (fileValidation == false && validUser == 0) {
				if ((inputText.isEmpty() == false)
						&& (ex.getSource() == currencyButton || ex.getSource() == currencyInput)) {
					switch (currencyOption.getSelectedIndex()) {
					case 0: // Euro
						num1 = 1.17;
						symbol = "€";
						break;
					case 1:
						// American Dollar
						num1 = 1.31;
						symbol = "$";
						break;
					case 2:
						// Australian Dollar
						num1 = 1.87;
						symbol = "$";
						break;
					case 3:
						// Canadian Dollar
						num1 = 1.71;
						symbol = "$";
						break;
					case 4:
						// IceLandic Krona
						num1 = 158.95;
						symbol = "kr";
						break;
					case 5:
						// united Arab Emirates Dirham
						num1 = 4.80;
						symbol = "د.إ";
						break;
					case 6:
						// // South African Rand
						num1 = 18.35;
						symbol = "R";
						break;
					case 7:
						// thai Baht
						num1 = 39.45;
						symbol = "฿";
						break;
					}
					try {
						num2 = Double.parseDouble(inputText);
					} catch (NumberFormatException e) {
						// if user didn't enter Integer value
						JOptionPane.showMessageDialog(null, "Please Input valid Number", "Error",JOptionPane.WARNING_MESSAGE);
						e.getMessage();
					}
					/**
					 * Decimal format and show result and count compopent 1
					 */
					DecimalFormat df = new DecimalFormat("##.##");
					if (MainPanel.chkReverse.isSelected() == false && checkReverse == true) {
						double result = num1 * num2;
						currenyResult.setText("Result : " + symbol + df.format(result));
						MainPanel.count++;
						MainPanel.counts.setText("Count Conversion  : " + MainPanel.count);
					} else if (MainPanel.chkReverse.isSelected() == true && checkReverse == true) {
						double result = num2 / num1;
						currenyResult.setText("£" + df.format(result));
						MainPanel.count++;
						MainPanel.counts.setText("Count No : " + MainPanel.count);
					} else {
						JOptionPane.showMessageDialog(null, "Please checkReverse selected should false", "Error",
								JOptionPane.INFORMATION_MESSAGE);
					}
				} else if ((inputText.isEmpty() == true)
						&& (ex.getSource() == currencyButton || ex.getSource() == currencyInput)) {
					// if no value in inputField display error message
					JOptionPane.showMessageDialog(null, "Please enter the  Validate number", "Error",
							JOptionPane.WARNING_MESSAGE);
				}

			}

		}
	}

	@SuppressWarnings({ "unchecked", "resource" })
	public static void loadCurrencyFile() {
		File file;
		JFileChooser chooser = new JFileChooser();
		int status = chooser.showOpenDialog(null);
		ArrayList<Currency> currencyList = new ArrayList<Currency>();
		if (status == JFileChooser.APPROVE_OPTION) {
			file = chooser.getSelectedFile();
			try {
				BufferedReader bf = new BufferedReader(new FileReader(file));
				Object[] lines = bf.lines().toArray();
				CurrencyPanel.currencyOption.removeAllItems();
				for (int i = 0; i < lines.length; i++) {
					String line = lines[i].toString();
					// System.out.println(line);
					CurrencyPanel.currencyOption.addItem(line.split(",")[0]);
					currencyList.add(convertCurrency(line));
				}

			} catch (Exception e) {
				e.printStackTrace();
			}

		}
		// System.out.println(currencyList);
	}

	/* reading file and push into arraylist to separate */
	private static Currency convertCurrency(String line) {
		if (line.contains(",")) {
			String country;
			String rate;
			String symbol;
			String[] value = line.split(",");
			if (value.length == 3) {
				country = value[0];
				rate = value[1];
				symbol = value[2];
				return new Currency(symbol, rate, country);
			} else {
				JOptionPane.showMessageDialog(null, "Warning", "UnSupported File Format....", 0);
			}
		}
		throw new IllegalArgumentException("File Format not Supported ..... :) ");
	}

}
