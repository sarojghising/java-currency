import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.text.DecimalFormat;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.UIManager;

/**
 * The main graphical panel used to display conversion components.
 * 
 * This is the starting point for the assignment.
 * 
 * The variable names have been deliberately made vague and generic, and most
 * comments have been removed.
 * 
 * You may want to start by improving the variable names and commenting what the
 * existing code does.
 * 
 * @author saroj ghising
 */
@SuppressWarnings("serial")
public class MainPanel extends JPanel {
	// Initialized String array with name list.
	private final static String[] list = { "Fahreheit to Celsius", "Miles per hour to Kilometres per hour",
			"Acres To Hectares", "Miles To Nautival Miles", "Yards To Metres", "Degrees To Radians" };

	// protected static final AbstractButton currencyInput = null;

	// define variable with swing components.

	private JTextField textField;
	private JPanel contentPane;
	private JLabel label;
	static JLabel counts;
	private JMenuItem show;
	public CurrencyPanel currencyPanel;
	private JComboBox<String> combo;
	private JLabel lblResult;
	private JLabel labelResult;
	// protected AbstractButton currenyResult;
	static JCheckBox chkReverse;
	@SuppressWarnings("unused")
	static String Path;
	static int count = 0;
	// Initialized count variable with integer data type

	@SuppressWarnings("deprecation")
	// JMenuBar function that added JmenuItems in menuBar
	JMenuBar setupMenu() {
		// Making object of JmenuBar
		JMenuBar menuBar = new JMenuBar();

		// adding JMenu items in JMenuBar
		JMenu m1 = new JMenu("File");
		JMenu m2 = new JMenu("Help");

		// adding JMenu Items in JMenuBar
		m1.setMnemonic(KeyEvent.VK_F);
		menuBar.add(m1);
		menuBar.add(m2);

		// adding sub JMenuItems in Help JMenu Items
		show = new JMenuItem("About");
		JMenuItem load = new JMenuItem("Load");

		// Key events set in show JMenu Items which works from keyboard shortcut.. CTRL
		// + S OR MAC => COMMAND + S
		show.setMnemonic(KeyEvent.VK_S);
		KeyStroke ctrlY = KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK);
		show.setAccelerator(ctrlY);
		show.addActionListener(null);

		// showing JOptionPane Dialog Box with Author Name and CopyRight after Click
		// show JMenuItem.
		show.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null,
						" \"Fahreheit to Celsius\",\"Miles per hour to Kilometres per hour\",\"Acres To Hectares\",\"Miles To Nautival Miles\",\"Yards To Metres\",\"Degrees To Radians\" Author : Saroj Ghising \u00a9 Copyright 2019");

			}
		});
		// load file code
		load.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CurrencyPanel.loadCurrencyFile();
			}
		});

		// adding show JMenuItems in JMenuBar
		m2.add(show);
		m1.add(load);
		////
		load.setMnemonic(KeyEvent.VK_L);
		KeyStroke ctrlL = KeyStroke.getKeyStroke(KeyEvent.VK_L, InputEvent.CTRL_MASK);
		load.setAccelerator(ctrlL);

		// adding JMenuItem with Exit world.
		JMenuItem item1 = new JMenuItem("Exit");
		// Key events set in show JMenu Items which works from keyboard shortcut ..
		// CTRIL + X OR MAC => COMMAND + X;
		item1.setMnemonic(KeyEvent.VK_X);
		KeyStroke ctrlX = KeyStroke.getKeyStroke(KeyEvent.VK_X, InputEvent.CTRL_MASK);
		item1.setAccelerator(ctrlX);
		// adding combo in items1`
		item1.addActionListener(combo);
		// setIcon in the exit MenuItems which attached JMenuBar
		item1.setIcon(new ImageIcon("/Users/sarojghising/Documents/Converter/src/9423a.ico"));
		load.setIcon(new ImageIcon("/Users/sarojghising/Documents/Converter/src/folder.png"));
		// adding Items in in JMenuItems
		m1.add(item1);
		// addActionListerner in exit Items that exit the frame of window
		item1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}

		});

		return menuBar;
	}

	@SuppressWarnings({})
	// Main Panel function
	MainPanel() {

		// ActionListener is to listener click events in coverterListener
		ActionListener listener = new ConvertListener();

		// combo is used to make DropDown Menu
		combo = new JComboBox<String>(list);
		combo.addActionListener(listener); // convert values when option changed
		combo.setBounds(20, 6, 180, 39); // setBounds is used to set x,y and width,height

		// label is set
		JLabel inputLabel = new JLabel("Enter Value : ");
		inputLabel.setForeground(Color.BLACK);
		inputLabel.setFont(new Font("", Font.BOLD, 12));

		// Converter button
		final JButton convertButton = new JButton("Convert");
		convertButton.setBounds(200, 300, 5000, 4000);
		convertButton.addActionListener(listener); // convert values when pressed
		convertButton.setBackground(Color.BLUE); // convert button color blue
		convertButton.setForeground(Color.BLACK); // convert text color white
		// set Font in Button
		convertButton.setFont(new Font("Tahoma", Font.BOLD, 18));
		convertButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				lblResult.setText(null);
			}
		});

		// Reset Button
		JButton btnReset = new JButton("Clear");
		// setToolTipText in reset button
		btnReset.setToolTipText("Press here to reset values");
		// set Mnemonic keyEvent
		btnReset.setMnemonic(KeyEvent.VK_R);
		// when button is press then to do these...
		btnReset.addActionListener(new ActionListener() {
			@SuppressWarnings("static-access")
			public void actionPerformed(ActionEvent arg0) {
				combo.setSelectedIndex(0); // reset combo index 0
				textField.setText(null); // setText should be null
				lblResult.setText(null); // setResult label be null
				counts.setText(null); // // counts setText is null
				textField.grabFocus(); // textField grabFocus null
				label.setText(null); // label setText null
				chkReverse.setSelected(false); // CheckBox if set Selected it if false
				labelResult.setText(null); // label Result should be null
				count = 0; // declare variable count = 0;
				counts.setText(String.valueOf(count)); // setText of count default value 0;
				currencyPanel.currencyInput.setText(null);
				currencyPanel.currenyResult.setText(null);
			}
		});
		// btnReset set Background , set Font , setForeGround and set Bounds.
		btnReset.setBackground(Color.MAGENTA);
		btnReset.setFont(new Font("Tahoma", Font.BOLD, 18));
		btnReset.setForeground(UIManager.getColor("ToolBar.Highlight"));
		btnReset.setBounds(273, 232, 119, 29);

		// exit button if it is click then frame will close
		JButton btnExit = new JButton("Exit");
		btnExit.setToolTipText("Press here to Exit");
		btnExit.setMnemonic(KeyEvent.VK_E);
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);

			}
		});
		// btnExit setBackground and setFont, setForeGround and setBounds
		btnExit.setBackground(SystemColor.textHighlight);
		btnExit.setFont(new Font("Tahoma", Font.BOLD, 18));
		btnExit.setForeground(UIManager.getColor("ToolBar.Highlight"));
		btnExit.setBounds(415, 232, 95, 29);

		// JLabel set
		label = new JLabel("---");
		counts = new JLabel("Conversion Count : ");
		counts.setForeground(SystemColor.BLACK);
		counts.setFont(new Font("Tahoma", Font.BOLD, 18));
		counts.setBounds(31, 400, 100, 100);

		// JLabel set
		lblResult = new JLabel();
		lblResult.setToolTipText("Your resutls will be published in this area");
		lblResult.setForeground(new Color(32, 178, 170));
		lblResult.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblResult.setBounds(31, 175, 398, 27);

		// JLabel set
		labelResult = new JLabel();
		labelResult.setToolTipText("Your resutls will be published in this area");
		labelResult.setForeground(Color.WHITE);
		labelResult.setFont(new Font("Tahoma", Font.BOLD, 18));
		labelResult.setBounds(31, 175, 398, 27);

		// Reverse CheckBox , setBounds
		chkReverse = new JCheckBox("Reverse Conversion");
		chkReverse.setToolTipText("select to reverse the operation");
		chkReverse.setBounds(415, 117, 95, 29);

		// set textField with set Bounds
		textField = new JTextField(5);
		// enter key press
		textField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					convertButton.doClick();
				}
			}
		});

		textField.setBounds(56, 42, 130, 26);
		// frame.getContentPane().add(textField);
		textField.setColumns(10);
		// adding all swing components
		add(combo);
		add(inputLabel);
		add(textField);
		add(convertButton);
		add(label);
		add(counts);
		add(lblResult);
		add(btnReset);
		add(btnExit);
		add(chkReverse);
		add(labelResult);

		// set Dimension for Frame
		setPreferredSize(new Dimension(800, 200));
		setBackground(Color.LIGHT_GRAY);

	}

	// convertListener function
	private class ConvertListener implements ActionListener {
		// action performed
		@Override
		public void actionPerformed(ActionEvent event) {
			// set value form inputField text
			String text = textField.getText().toString().trim();
			// set CheckBox if it is selected
			boolean b = chkReverse.isSelected();
			// DropdownMenu Selected Index
			int get = combo.getSelectedIndex();
			// double mphKph = 0;
			// text empty
			if (text.isEmpty()) {
				lblResult.setText("<html><font color = RED>Please enter a value first</font></html>");
				return;
			} else {
				/*
				 * if DropDown Selected then switch case executed.. I made method that have 2
				 * arguments one is String text and another is CheckBox if it is selected.
				 */
				switch (get) {
				case 0:
					fahToCs(text, b);
					break;
				case 1:
					mphKpH(text, b);
					break;
				case 2:
					acresHectares(text, b);
					break;
				case 3:
					milesNauticalM(text, b);
					break;
				case 4:
					yardsMetres(text, b);
					break;
				case 5:
					degreeRadian(text, b);
					break;
				}
			}

		}
		// method is declare with 2 arguments one is String that comes from TextField
		// and another is CheckBox is Selected which is boolean.

		private void degreeRadian(String s, boolean b) {
			// TODO Auto-generated method stub
			double value = 0.0;
			try {
				value = Double.parseDouble(s);

			} catch (NumberFormatException e1) {
				JOptionPane.showMessageDialog(null, "Enter Validate Number");
				return;

			}
			if (b) {
				// 1ha = value * 2.471;
				double result = (value * (180 / Math.PI));
				DecimalFormat dc = new DecimalFormat("##.###");
				String formattedText = dc.format(result);
				labelResult.setText("Radians To Degree :  " + String.valueOf(formattedText));

			} else {
				//
				double result = (value * (Math.PI / 180));
				DecimalFormat dc = new DecimalFormat("##.###");
				String formattedText = dc.format(result);
				label.setText(String.valueOf(formattedText));
			}

			count++;
			counts.setText(String.valueOf(count));
			counts.setFont(new Font("Tahoma", Font.BOLD, 18));
			counts.setForeground(SystemColor.BLUE);

		}

		private void yardsMetres(String s, boolean b) {
			// TODO Auto-generated method stub
			double value = 0.0;
			try {
				value = Double.parseDouble(s);

			} catch (NumberFormatException e1) {
				JOptionPane.showMessageDialog(contentPane, "Enter Validate Number");
				return;
			}
			if (b) {
				// 1ha = value * 2.471;
				double result = value * 1.094;
				DecimalFormat dc = new DecimalFormat("##.###");
				String formattedText = dc.format(result);
				labelResult.setText("Metres To Yards :  " + String.valueOf(formattedText));

			} else {
				//
				double result = (value / 1.094);
				DecimalFormat dc = new DecimalFormat("##.###");
				String formattedText = dc.format(result);
				label.setText(String.valueOf(formattedText));
			}

			count++;
			counts.setText(String.valueOf(count));
			counts.setFont(new Font("Tahoma", Font.BOLD, 18));
			counts.setForeground(SystemColor.BLUE);

		}

		private void milesNauticalM(String s, boolean b) {
			// TODO Auto-generated method stub
			double value = 0.0;
			try {
				value = Double.parseDouble(s);

			} catch (NumberFormatException e1) {
				JOptionPane.showMessageDialog(contentPane, "Enter Validate Number");
				return;
			}
			if (b) {
				// 1ha = value * 2.471;
				double result = value * 1.151;
				DecimalFormat dc = new DecimalFormat("##.###");
				String formattedText = dc.format(result);
				labelResult.setText("Miles To MilesNautical :  " + String.valueOf(formattedText));

			} else {
				//
				double result = (value / 1.151);
				DecimalFormat dc = new DecimalFormat("##.###");
				String formattedText = dc.format(result);
				label.setText(String.valueOf(formattedText));
			}

			count++;
			counts.setText(String.valueOf(count));
			counts.setFont(new Font("Tahoma", Font.BOLD, 18));
			counts.setForeground(SystemColor.BLUE);

		}

		private void acresHectares(String s, boolean b) {
			// TODO Auto-generated method stub
			double value = 0.0;
			try {
				value = Double.parseDouble(s);

			} catch (NumberFormatException e1) {
				JOptionPane.showMessageDialog(contentPane, "Enter Validate Number");
				return;
			}
			if (b) {
				// 1ha = value * 2.471;
				double result = value * 2.471;
				DecimalFormat dc = new DecimalFormat("##.###");
				String formattedText = dc.format(result);
				labelResult.setText("Hectares To Acres :  " + String.valueOf(formattedText));

			} else {
				//
				double result = (value / 2.471);
				DecimalFormat dc = new DecimalFormat("##.###");
				String formattedText = dc.format(result);
				label.setText(String.valueOf(formattedText));
			}

			count++;
			counts.setText(String.valueOf(count));
			counts.setFont(new Font("Tahoma", Font.BOLD, 18));
			counts.setForeground(SystemColor.BLUE);

		}

		private void mphKpH(String s, boolean b) {
			double value = 0.0;
			try {
				value = Double.parseDouble(s);

			} catch (NumberFormatException e1) {
				JOptionPane.showMessageDialog(contentPane, "Enter Validate Number");
				return;
			}

			if (b) {
				// 1kilometer = 0.621371192 miles
				double result = value * 0.621371192;
				DecimalFormat dc = new DecimalFormat("##.###");
				String formattedText = dc.format(result);
				labelResult.setText("Kph To Mph :  " + String.valueOf(formattedText));

			} else {
				double result = value * 1.60934;
				DecimalFormat dc = new DecimalFormat("##.###");
				String formattedText = dc.format(result);
				label.setText(String.valueOf(formattedText));

			}
			count++;
			counts.setText(String.valueOf(count));
			counts.setFont(new Font("Tahoma", Font.BOLD, 18));
			counts.setForeground(SystemColor.BLUE);

		}

		private void fahToCs(String s, boolean b) {
			double value = 0.0;
			try {
				value = Double.parseDouble(s);

			} catch (NumberFormatException e1) {
				JOptionPane.showMessageDialog(contentPane, "Enter Validate Number");
				return;
			}

			if (b) {
				// System.out.println("reverse");
				double result = (value * 9 / 5) + 32;
				DecimalFormat dc = new DecimalFormat("##.###");
				String formattedText = dc.format(result);
				labelResult.setText("Celsius To Fahreheit : " + String.valueOf(formattedText));

			} else {
				double result = (value - 32) * 5 / 9;
				DecimalFormat dc = new DecimalFormat("##.###");
				String formattedText = dc.format(result);
				label.setText(String.valueOf(formattedText));

			}
			count++;
			counts.setText(String.valueOf(count));
			counts.setFont(new Font("Tahoma", Font.BOLD, 18));
			counts.setForeground(SystemColor.BLUE);

		}
	}

}

// f to c
