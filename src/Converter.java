import java.awt.Color;
import java.awt.GridLayout;
import java.awt.SystemColor;
import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.border.Border;

/**
 * The main driver program for the GUI based conversion program.
 * 
 * @author saroj ghising
 */
public class Converter {
    public static void main(String[] args) {

        // Name of frame
        JFrame frame = new JFrame("Converter");

        // setting default closing operation in JFrame
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // Border borderTemp = BorderFactory.createMatteBorder(5, 5, 1, 5, Color.BLACK);
        Border borderCurrency = BorderFactory.createMatteBorder(1, 5, 5, 5, Color.BLACK);

        // set width,height and x , y in frame
        frame.setSize(1500, 200);
        // frame.setSize(500,500);

        /// panel of frame that has MainPanel
        MainPanel panel = new MainPanel();

        // adding border with title
        panel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10),
                "Converter Temperature"));
        // adding component border

        // adding currency
        CurrencyPanel currency = new CurrencyPanel();
        currency.setBorder(BorderFactory.createCompoundBorder(borderCurrency,
                BorderFactory.createTitledBorder(BorderFactory.createEmptyBorder(), "Currency Converter")));
        // currency.setBackground(Color.BLACK);

        // color set in background
        panel.setBackground(SystemColor.ORANGE);
        // adding color to panel
        currency.setBackground(SystemColor.CYAN);

        // using grid_layout with 2 rows and column
        frame.setLayout(new GridLayout(2, 1));
        // setupMenu function call inside panel to set JMenuBar
        frame.setJMenuBar(panel.setupMenu());
        // adding panel in frame
        frame.getContentPane().add(panel);
        frame.getContentPane().add(currency);
        // pack in frame and setVisible true to see output
        frame.pack();
        frame.setVisible(true);
        // frame.contains(null);
    }
}
