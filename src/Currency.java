public class Currency {
	private String name;
	private String factory;
	private String symbol;
	Currency(String name, String rate, String symbol) {
		this.setName(name);
		this.setFactory(rate);
		this.setSymbol(symbol);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFactory() {
		return factory;
	}

	public void setFactory(String rate) {
		this.factory = rate;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}
}
